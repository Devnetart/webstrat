<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'webstrat');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

// ** MY CUSTOM WORDPRESS SETTINGS ** //
define('WP_HOME', 'http://' . $_SERVER['SERVER_NAME']. '/demoWebStrat');
define('WP_HOME_DIR', dirname(__FILE__));
define('WP_SITEURL', WP_HOME . '/wordpress');
define('WP_CONTENT_URL', WP_HOME . '/wp-content');
define('WP_CONTENT_DIR', WP_HOME_DIR . '/wp-content');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'idl:sIrJupowblyA` q@%QIp4X8`VmKj>V^LTX$m{X;m}gFcXQvm}KD027JI2rb>');
define('SECURE_AUTH_KEY',  'Yl!*skq<+H7|qaOMCeue>#)tT^!C+%h*_XS}B,px#@ls-3zjM|R$LkOdU_tk,EgQ');
define('LOGGED_IN_KEY',    'EG[vr43-d^eu1eJ8``jySlnm)xg3Bl .&e?F%^8t?EybW9ohS/ZXc!]M@#$$Zf.g');
define('NONCE_KEY',        'J%W*X!8= `= d;_?X/LfY%/<yI7=Ypz<ub-p$q5hZItZBxfU#u00(p}9DZxzdf>2');
define('AUTH_SALT',        '.7[l0k/Mgzrnd?N!/.vaUBEC*B?*&al7w3JI5IS7AH-7Tvehe/UF1<va}?U w:=1');
define('SECURE_AUTH_SALT', '}=;OEyJnX>GJCVZoa*qYhr $;p[azL@W|Qx(jW4(uJ|.B#9)N~iF4N>.~wYw%jZ1');
define('LOGGED_IN_SALT',   'm.HG!mo?T)-bwA)Vtwo` o!%A]4 h-Qfyc XEVVOKMyn{uk <2g;.H7z]E.()p,>');
define('NONCE_SALT',       '4g~T;g%83xinS|pt-U (INY,d,3U&&t^ik .P,kQr9Zt[.EXnxD_j87[oO/~-}Oc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
