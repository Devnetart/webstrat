<?php
/*
*	1 - Enqueue all styles and scripts required
*	2 - Generateur CPT et Taxonomies
*	3 - SDK FACEBOOK
*/

/***********************************************/
/* 1 - Enqueue all styles and scripts required */
function enqueue_styles() {
	// Style parent
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	// Load fontawesome
	wp_enqueue_style( 'fontawesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css', array(), '4.0.3' );
	// Load facebook sdk
	//wp_enqueue_script( 'facebook-sdk', get_stylesheet_directory_uri() . '/js/facebook-sdk.js', array(), '1.0.0', true );
	// Load Goole maps dependances
	//wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDqCKWgErXwrAYkq7gQYPfTPT4sJq-pbps', array(), '3', true );
	//wp_enqueue_script( 'google-map-init', get_stylesheet_directory_uri() . '/js/google-maps.js', array('google-map', 'jquery'), '0.1', true );

}
add_action( 'wp_enqueue_scripts', 'enqueue_styles' );

/************************************************/
/* 		2 - Generateur CPT et taxonomies    	*/
function custom_post_types() {
	// 1 - GENERATE CUSTOM POSTS TYPES
	/** 
	* Fonction pour générer les CPT sans avoir à retaper le code...
	*	@param $cpt = Libelle du type de contenu
	*	@param $genre = Tableau pour le genre du mot : Masculin, feminin
	*	@param $position = position dans le menu
	*/
	function generate_cpt_args($cpt, $genre, $position){
		// 				array(MajusculeSingulier - singulier - MajusculePluriel - pluriel)
		$newcpt = array(ucfirst($cpt), lcfirst($cpt), ucfirst($cpt).'s', lcfirst($cpt).'s');
		$labels = array(
			'name'                  => $newcpt[2],
			'singular_name'         => $newcpt[0],
			'menu_name'             => $newcpt[2],
			'name_admin_bar'        => $newcpt[2],
			'parent_item_colon'     => 'Élément parent:',
			'all_items'             => $genre[0].$newcpt[3],
			'add_new_item'          => 'Ajouter '.$genre[1].$newcpt[1],
			'add_new'               => 'Ajouter '.$newcpt[1],
			'new_item'              => $genre[2].$newcpt[1],
			'edit_item'             => 'Modifier '.$genre[3].$newcpt[1],
			'update_item'           => 'Mettre à jour '.$genre[3].$newcpt[1],
			'view_item'             => 'Voir '.$genre[3].$newcpt[1],
			'search_items'          => 'Chercher '.$genre[1].$newcpt[1],
			'not_found'             => 'Introuvable',
			'not_found_in_trash'    => 'Introuvable dans la corbeille',
			'items_list'            => 'Liste des '.$newcpt[3],
		);

		$args = array(
			'label'                 => $newcpt[0],
			'description'           => 'Un type de contenu dédié aux'.$newcpt[3],
			'labels'                => $labels,
			'supports'              => array('title', 'thumbnail'),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => $position,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => $newcpt[3],
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'post',
		);
		return $args;
	}

	// 2 - GENERATE TAXONOMYS
	/**
	*	Fonction pour générer les taxonomies associées aux CPT sans avoir à retaper le code...
	*	@param $name = Libelle de la nouvelle catégorie
	*/
	function generate_taxomy_args($name) {
		$taxoLabels = array(
			'name'                       => _x( 'Categories '.$name.'', 'Taxonomy General Name', 'devart' ),
			'singular_name'              => _x( 'Catégorie', 'Taxonomy Singular Name', 'devart' ),
			'menu_name'                  => __( 'Catégorie', 'devart' ),
			'all_items'                  => __( 'Toutes les catégories', 'devart' ),
			'parent_item'                => __( 'Catégorie parent', 'devart' ),
			'parent_item_colon'          => __( 'Catégorie parent:', 'devart' ),
			'new_item_name'              => __( 'Nouvelle catégorie', 'devart' ),
			'add_new_item'               => __( 'Créer catégorie', 'devart' ),
			'edit_item'                  => __( 'Editer catégorie', 'devart' ),
			'update_item'                => __( 'Modifier la catégorie', 'devart' ),
			'view_item'                  => __( 'Voir la catégorie', 'devart' ),
			'separate_items_with_commas' => __( 'Séparer les catégories d\'une virgule', 'devart' ),
			'add_or_remove_items'        => __( 'Ajouter ou supprimer catégorie', 'devart' ),
			'choose_from_most_used'      => __( 'Choisir parmi les plus utilisées', 'devart' ),
			'popular_items'              => __( 'Catégories populaires', 'devart' ),
			'search_items'               => __( 'Chercher catégorie', 'devart' ),
			'not_found'                  => __( 'Aucune catégorie trouvée', 'devart' )
		);

		$args = array(
			'labels'                    => $taxoLabels,
			'hierarchical'              => true,
			'public'                    => true,
			'show_ui'                   => true,
			'show_admin_column'         => true,
			'show_in_nav_menus'         => true,
			'show_tagcloud'             => true,
			'rewrite'					=> array('slug' => $name)
		);
		return $args;
	}

	/***************************************************************/
	/* UNIQUE BOUT DE CODE A MODIFIER POUR GENERER DES CPT ET TAXO */
	// Tableau des pronoms et articles pour le genre des mots
	static $masculin = array("Tous les ", "un ", "Nouveau ", "le ");
	static $feminin = array("Toutes les ", "une ", "Nouvelle ", "la ");
	
	// Tableau des CPT et de leur taxonomy à créer
	$array_cpt = array(
		array("boisson", $feminin),
		array("snack", $masculin),
		array("evenement", $masculin),
		array("actualite", $feminin),
		array("partenaire", $masculin)
	);

	/* Logique d'auto-génération des CPT en fonction du tableau précédent */
	for ($i=0; $i < count($array_cpt); $i++) {
		// Gère la pluralité et la casse des charactères
		$temp_cpt = array(ucfirst($array_cpt[$i][0]), lcfirst($array_cpt[$i][0]), ucfirst($array_cpt[$i][0]).'s', lcfirst($array_cpt[$i][0]).'s');
		// Générer les CPT en prenant garde au genre du mot (féminin - masculin) et les sauvegarder
		register_post_type( $temp_cpt[3], generate_cpt_args($temp_cpt[1], $array_cpt[$i][1], $i+2) );
		// Générer les taxonomies associées
		register_taxonomy('categories_'.$temp_cpt[3], 	 $temp_cpt[3]	, generate_taxomy_args($temp_cpt[2]));
	}
	/***************************************************************/

}
add_action( 'init', 'custom_post_types', 0 );


/*******************************************************/
/*  3 - Add the fb Javascript sdk for sharing content  */
add_action( 'wp_footer', 'x_print_fb_sdk' );
function x_print_fb_sdk(){
	?>
	<div id="fb-root"></div>
	<script>	
		(function(d, s, id) {
	  		var js, fjs = d.getElementsByTagName(s)[0];
	  		if (d.getElementById(id)) return;
	  		js = d.createElement(s); js.id = id;
	  		js.src = "//connect.facebook.net/fr_FR/sdk.js";
	  		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<?php
}